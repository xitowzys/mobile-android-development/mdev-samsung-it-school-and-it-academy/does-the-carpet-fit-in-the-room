fun main() {
    val (a, b, m, n) = readLine()!!.split(' ').map(String::toFloat)

    println(if ((a >= m && b >= n) || (a >= n && b >= m)) "YES" else "NO")
}